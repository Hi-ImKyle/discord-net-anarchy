This is basically Discord .Net but I added some more features (back) like user login, some stuff in Group and DM-Channels, etc.
All the changes that i made are listed below


WHAT I CHANGED:

DiscordSocketClient.cs:244 -> added validation to check if its a user or bot token

DiscordSocketClient.cs:908 -> data.Application is null for users


DiscordRestApiClient.cs:45 -> add setter and Call SetBaseUrl() in there

DiscordRestApiClient.cs:96 -> Add User and webhook to option and just return token without anything else


IGuild.cs:796 -> created CreateNewsChannelAsync method


TokenType.cs:23 -> Added user token type


TokenUtils.cs:166 -> removed error message of user token


SocketGuild.cs:642 -> Made User nullable in GetBansAsync

SocketGuild.cs:1503 -> removed check if there are stickers in GetStickersAsync()

SocketGuild.cs:843,1941 -> Created CreateNewsChannelAsync() method (+ implementation of IGuild)


GuildHelper.cs:354 -> Changed max of 5 to 20 in forum tags

GuildHelper.cs:403 -> created CreateNewsChannelAsync method


ChannelHelper.cs:400 -> fixed user problem with upload (aka added isbot check)


ITextChannel.cs:11 -> Added interface IWebhookChannel

ITextChannel.cs:182 -> Added GetArchivedThreadsAsync method


SocketTextChannel.cs:139 -> Added GetArchivedThreadsAsync method

SocketTextChannel.cs:146 -> Added GetAllThreadChannels method which gets both active an archived thread channels

SocketTextChannel.cs:406 -> Implemented GetArchivedThreadsAsync method


RestTextChannel.cs:297  -> Added GetArchivedThreadsAsync method

RestTextChannel.cs:337 -> Implemented GetArchivedThreadsAsync method


ThreadHelper.cs:99 -> added GetArchivedThreads method


ForumHelper.cs:39 -> added id so you can actually update tags without deleting them first

ForumHelper.cs:106 -> Changed max of 5 to 20 in forum tags


ForumTagProperties.cs:21 -> Added id to the constructor


ForumTag.cs:53 -> Added .Build() method for easier access


SocketCustomSticker.cs:50 -> added override so i can actually have the fucking author of the shitty sticker


IGroupChannel.cs:19 -> added IUser Owner and string GetIconUrl()


ISocketPrivateChannel.cs:12 -> Added Users and CurrentUser


RestGroupChannel.cs:20 -> renamed iconId  to ownerId

RestGroupChannel.cs:23 -> Created public IconsId prop 

RestGroupChannel.cs:30 -> Added Owner Property

RestGroupChannel.cs:33 -> fixed Users get

RestGroupChannel.cs:44 -> fixed Recipients get

RestGroupChannel.cs:62 -> fixed IconId

RestGroupChannel.cs:64 -> assigned ownerId field

RestGroupChannel.cs:94 -> Added GetIconUrl


SocketGroupChannel.cs:27 -> renamed iconId to ownerId

SocketGroupChannel.cs:30 -> Created public IconsId prop 

SocketGroupChannel.cs:44 -> Added Owner Property, redid Users Get and fixed Recipients Get

SocketGroupChannel.cs:49 -> fixed Users get

SocketGroupChannel.cs:63 -> fixed Recipients get

SocketGroupChannel.cs:85 -> fixed IconId

SocketGroupChannel.cs:87 -> assigned ownerId field

SocketGroupChannel.cs:112 -> Added GetIconUrl

SocketGroupChannel.cs:346 -> Added implementations of Users and CurrentUser


SocketDMChannel.cs:254 -> Added implementations of Users and CurrentUser


RestGuild.cs:347 -> Made User nullable in GetBansAsync

RestGuild.cs:726,1404 -> Created CreateNewsChannelAsync() method (+ implementation of IGuild)


RestGuildEvent.cs:109 -> fixed GetCoverImageUrl() method


SocketGuildEvent.cs:120 -> fixed GetCoverImageUrl() method


CDN.cs:220 -> Fixed event url (removed guildid) + fixed nullreference exception


FileAttachment.cs:66 -> added new constrcutor that instead of taking a stream takes a byte[] of data


SocketInteractionContext.cs:16,49,92 -> Replaced DiscordSocketClient with BaseSocketClient


DiscordShardedClient.cs:296 -> fixed null exception if sticker is from an unknown guild


IForumChannel.cs:10 -> Added Interface IWebhookChannel

IForumChannel.cs:244 -> added GetArchivedThreadsAsync method


SocketForumChannel.cs:17 -> added DebuggerDisplay attribute

SocketForumChannel.cs:100 -> made CategoryId nullable

SocketForumChannel.cs:148 -> Added GetArchivedThreadsAsync method

SocketForumChannel.cs:167 -> Added GetThreadChannels method which tries to get both active and archived threads from Discord

SocketForumChannel.cs:195 -> Implemented GetArchivedThreadsAsync method

SocketForumChannel.cs:176 -> added DebuggerDisplay and Clone

SocketForumChannel.cs:234 -> Added methods for webhooks from IWebhookChannel


RestForumChannel.cs:92 -> made CategoryId nullable

SocketForumChannel.cs:208 -> Added methods for webhooks from IWebhookChannel


IWebhookChannel.cs -> Added class + methods for webhook creation and to get webhooks and the whole fucking tail that comes with it (hf implementing that next time kekW)