using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test123
{
    public class InteractionModule : InteractionModuleBase<SocketInteractionContext>
    {

        [ComponentInteraction("dismami")]
        public async Task Ping()
        {
            await DeleteOriginalResponseAsync();
            await GetOriginalResponseAsync();
        }

        [SlashCommand("testerussi", "a")]
        public async Task TestCommand()
        {
            var component = new ComponentBuilder().WithButton("test", "dismami").Build();

            await RespondAsync("a", components: component, ephemeral: true);
        }
    }


    [Flags]
    public enum BackupFlag
    {
        Incremental = 1, //structure, new messages
        Structure = 2, //structure only
        Full = 4, //structure, update all messages
        NoThreads = 1024,
        [Hide]
        StructureNoThreads = 65536, //structure only and no Threads
    }

    public enum TestEnum
    {
        Choice1,
        Choice2,
        Choice3,
    }
}
