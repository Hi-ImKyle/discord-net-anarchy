using Discord;
using Discord.Interactions;
using Discord.Rest;
using Discord.Webhook;
using Discord.WebSocket;
using Microsoft.VisualBasic;
using Test123;

internal class Program
{
    static DiscordShardedClient client;
    static InteractionService interactions;

    static void Main(string[] args)
    {
        client = new DiscordShardedClient(new DiscordSocketConfig()
        {
            LogLevel = Discord.LogSeverity.Error,
            AlwaysDownloadDefaultStickers = true,
            TotalShards = 1,

        });

        try
        {
            client.Log += Client_Log;
            client.ShardReady += Client_Ready;
            client.StartAsync().Wait();

            client.ButtonExecuted += ButtonExecuted;
            client.SlashCommandExecuted += SlashCommandExecuted;

            interactions = new InteractionService(client, new InteractionServiceConfig()
            {
                DefaultRunMode = RunMode.Async,
                InteractionCustomIdDelimiters = new char[] { ' ' },
                UseCompiledLambda = true,
                EnableAutocompleteHandlers = true,

            });


            Task.Delay(Timeout.Infinite).Wait();
        }
        catch (Exception ex)
        {
            var a = ex;
        }
    }



    private async static Task Client_Ready(DiscordSocketClient e)
    {
        try
        {

            interactions.AddModuleAsync(typeof(InteractionModule), null).Wait();
            await interactions.RegisterCommandsGloballyAsync();
            //var user = await (await client.Rest.GetUserAsync(445637302320889857)).CreateDMChannelAsync();
            //
            //var builder = new ComponentBuilder()
            //    .AddRow(new ActionRowBuilder()
            //    .AddComponent(new ButtonBuilder("a", "test dismami").Build()))
            //    .Build();
            //
            //await user.SendMessageAsync("a", components: builder);

        }
        catch (Exception ex)
        {

        }
    }

    private async static Task ButtonExecuted(SocketMessageComponent arg)
    {
        var a = await interactions.ExecuteCommandAsync(new SocketInteractionContext(client, arg), null);
        var b = a;
    }

    private async static Task SlashCommandExecuted(SocketSlashCommand arg)
    {
        var a = await interactions.ExecuteCommandAsync(new SocketInteractionContext(client, arg), null);
        var b = a;
    }



    private async static Task Client_Log(Discord.LogMessage arg)
    {
        Console.WriteLine(arg.Message);
    }
}
