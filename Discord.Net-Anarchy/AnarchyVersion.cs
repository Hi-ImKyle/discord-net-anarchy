namespace Discord.Net_Anarchy
{
    public class AnarchyVersion
    {
        public static string GetVersion() => "Discord.Net-Anarchy v1.4.3 by HeroYT";

        public static string GetDiscordVersion() => "v3.9.0";
    }
}
